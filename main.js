const cl = console.log;

const Root = {
  // todo
}

const Area = {
    start: function() {
        Element = document.createElement('DIV');
        Element.setAttribute('id', 'arrow');
        Element.setAttribute('class', 'arrow');
        Element.textContent = '^';
        document.body.appendChild(Element);
    },
    interval: setInterval(updateArea, 20),
    stop: function() { clearInterval(this.interval) }
}
const calculateXY = (direction) => ({
    x: Math.sin(direction * (Math.PI / 180)),
    y: Math.cos(direction * (Math.PI / 180))
});

const Component = {
    direction: 0,
    position: {
        y: 500,
        x: 200
    },
    speedX: 0,
    speedY: 0,
    gravity: 0.5,
    update: function() {
        document.getElementById('arrow')
            .style.transform = "translate(" + this.position.x + "px," + this.position.y + "px) rotate(" + this.direction + "deg)";
    },
    newPos: function(map) {

        // comments
        if(this.speedX > 0) {
            this.speedX -= 0.1;
            this.speedY -= 0.1;
        }
        if(this.speedX < 0) {
            this.speedX += 0.1;
            this.speedY += 0.1;
        }

        cl(this.position.y >= document.body.clientHeight);
        cl(this.position.y <= 0);

        cl(this.position.x >= document.body.clientWidth);
        cl(this.position.x >= 0);

        if(window.innerHeight <= this.position.y |
            this.position.y |
            1 ) {
            this.reverse = true;
        }

        this.position.x += calculateXY(this.direction).x * this.speedX;
        this.position.y -= calculateXY(this.direction).y * this.speedY;

        if(map['ArrowUp']){
            if(this.gravity >= 0) {
                this.reverse = false;
                this.speedX += this.gravity;
                this.speedY += this.gravity;
            }
        }

        if(map['ArrowDown']){
            if(this.gravity >= 0) {
                this.speedX -= this.gravity;
                this.speedY -= this.gravity;
            }
        }

        if(map['ArrowRight']){
            this.direction = (this.direction === 360) ? 0 : this.direction += this.speedX;
        }

        if(map['ArrowLeft']){
            this.direction = (this.direction === 0) ? 360 : this.direction -= this.speedY;
        }
    }
};

let map = {};
const onKeyDown = onKeyUp = (e) => {
    map[e.key] = e.type === 'keydown';
}

document.body.addEventListener('keydown', (e) => onKeyDown(e));
document.body.addEventListener('keyup', (e) => onKeyUp(e));

function updateArea() {
    Component.newPos(map);
    Component.update();
};

Area.start();
